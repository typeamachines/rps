version-control-readme.md

Or lack thereof. The files in this
subdirectory are listed as being version
5.1 when in reality, these files are
more up-to-date than version 5.2.

These files were added and listed as current
at the request of Manufacturing Operations
on March 14 2017, and added to the git repo
on March 27.

These files are possessed by BriteLab as well
as Type A Machines.
