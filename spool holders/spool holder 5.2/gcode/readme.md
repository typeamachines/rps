##############################################
########## IMPORTANT -- PLEASE READ ##########
##############################################
#
# Which .gcode file should I print? Read on to
# find out, or if this is confusing, download
# some pre-made packages from:
#
# http://www.typeamachines.com/downloads/
#
# The proper gcode file to use can
# be determined using the filename:
#
# Spool-Holder-5.1_[part]_[nozzle]_[quantity]
#
# [part] - Which part is in the gcode.
# 001 = Base
# 002 = 50mm Adaptor
# 003 = 30mm Adaptor
# 004 = Nut
# 002-003 = 30mm Adaptor and 50mm Adaptor
#
# [nozzle] - For what nozzle diameter is the
# .gcode file sliced? If unsure, start with 0.4.
# 04 = 0.4mm Nozzle, stanadard on all Series 1s
#      as of Dec 2016.
# 06 = 0.6mm Nozzle
#
# [quantity] - How many copies?
# (nothing) = only one item or group of items.
# 1x = same as '(nothing)'
# 2x = two copies of the item or plate of items.
# 3x = three copies, etcetera.
#
# Example:
# filename:  'spool-holder-5.1_001-004_06_2x.gcode'
#  meaning:   A file containing gcode for two
#             copies of the base and nut, sliced
#             for a 0.6mm nozzle.
