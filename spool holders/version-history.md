# Some basic version history information
# for the sake of posterity.
#
# As of December 2016, the version number of
# the spool holder is described as "5.2".
#
# There has never been a clear line of versioning
# that defines one version of the spool holder
# from another. This document is problaby the
# most comprehensive document on the subject.
#
# "1.0"
# The original verison of the spool holder
# shipped in late 2011 and into the middle
# of 2013. It was the used in the 2012 MAKE:
# Magazine printer showdown. The Series 1 won
# "Best In Class - Midrange" that year.
#
# There was a lot of criticism of that spool,
# and its proclivity to, well, fall clean off
# the machine if there was a filament
# entanglement, whcih was much more common in
# those days. They also tended to break during
# shipping, and eventually were replaced.
#
# "2.0 / The Top Hat"
# The Top Hat was one of the Marquee features
# of the later batch of the 2013 Series 1 in
# order to justify a price increase that was
# the result of increasing costs of goods sold.
# To add a little context, most of the 3D
# printing industry were increasing prices at
# this time in response to increasings COGS.
#
# The Top Hat has the same footprint of the
# Series 1, and sat atop it. A dowell allowed
# for hanging up to four spools at a time, which
# made it good as a partial storage solution.
#
# "3.0"
#
# Version 3.0 of the spool holder was the version
# that shipped with the majority of the 2014
# Series 1 3D printers. It included a metal
# L-shaped bracket, and had some similarities
# with version 1.0 - but had improvements
# including a slot on the guide hole making it
# possible to remove the filament from the guide
# without having to take the filament out of the
# extruder. It too had issues with tangles, but
# much less so than its predecessors. The largest
# criticism was that it could not support every
# diameter of spool, an issue solved in later
# versions.
#
# "4.0 / The U-Spool Holder"
#
# The U-spool holder, sometimes called around the
# office, the toilet-roll spool holder, was a
# total redesign which began to ship in early
# 2015. It featured a U-shape with a thin
# removable hub made from either a metal smooth
# rod or part of a pencil. The U-spool holder,
# because of its insanely tiny hole diameter,
# could support any hub diameter. It could not
# support a guide tube despite one being included
# for the first time in a Type A spool holder.
# It did have issues with the spool falling off
# under certain circumstances, however it was a
# vast improvement over previous versions.
#
# "5.0 / YASH"
# Version 5, the current version that we're using
# is also known as YASH:
#
# [Y]et
# [A]nother
# [S]pool
# [H]older
#
# The YASH design is the most comprehensively
# compatable design we've ever shipped. It takes
# advantage of the fact that spool hub diameters
# have more or less standardized into 3 different
# sizes. It also takes advantage of the fact that
# we can print much more precicely than before, and
# allows us to print the object faster despite the
# large volume of the printed parts. We have kind of
# a changelog from this point. Kind of:
#
# 5.0 - Inital design, shipped with Print Pods only.
# 5.1 - Optimised dims and other issues.
# 5.2 - made the adapters solid to print faster.
# 5.3 - cleans up design and filenames.