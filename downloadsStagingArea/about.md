This is a staging area used to create downloads
for those uncomfortable with using git.

The zip files can be downloaded from the Type A
website at http://www.typeamachines.com/downloads/

Basically, you can ingore this directory.
